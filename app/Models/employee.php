<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class employee extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'firstName', 'lastName', 'email', 'phone', 'user_id', 'company_id'
    ];

    public function company()
    {
        return $this->hasOne('App\Models\company',  'id', 'company_id');
    }
}