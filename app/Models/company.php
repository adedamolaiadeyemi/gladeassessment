<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\employee;
use Illuminate\Database\Eloquent\SoftDeletes;
use OwenIt\Auditing\Contracts\Auditable;

class company extends Model implements Auditable
{
    use HasFactory, SoftDeletes, \OwenIt\Auditing\Auditable;


    protected $fillable = [
        'name', 'email', 'logo', 'website', 'user_id'
    ];

    public function employees()
    {
        return $this->hasMany('App\Models\employee', 'company_id', 'id');
    }
}