<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('The Primary Key for the table.');
            $table->string('name', 100)->unique('name')->nullable(false)->comment('Company Name');
            $table->string('email', 50)->unique('email')->nullable(false)->comment('Company Email');
            $table->string('logo', 200)->comment('Company logo path')->nullable(true);
            $table->string('website', 100)->comment('Company logo path')->nullable(true);
            $table->unsignedBigInteger('user_id')->index('user_id')->comment('The corresponding user.');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');;
        });


        Schema::create('employees', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('The Primary Key for the table.');
            $table->string('firstName', 50)->nullable(false)->comment('employees first Name');
            $table->string('lastName', 50)->nullable(true)->comment('employees last Name');
            $table->string('email', 50)->unique('email')->nullable(false)->comment('employees Email');
            $table->string('phone', 20)->nullable(false)->nullable(true)->comment('employees last Name');

            $table->unsignedBigInteger('user_id')->index('user_id')->comment('The corresponding user.');
            $table->unsignedBigInteger('company_id')->index('company_id')->comment('The corresponding company.');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
        });
    }



    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
        Schema::dropIfExists('employees');
    }
}
