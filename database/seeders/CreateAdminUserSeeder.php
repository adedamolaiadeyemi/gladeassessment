<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;




use DB;
use Hash;
use Illuminate\Support\Arr;





class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'Super Admin',
            'email' => 'superadmin@admin.com',
            'password' => bcrypt('password')
        ]);



        $role = Role::create(['name' => 'superadmin']);

        $permissions = Permission::pluck('id', 'id')->all();

        $role->syncPermissions($permissions);

        // $user->assignRole([$role->id]);



        $roleAdmin = Role::create(['name' => 'admin']);
        $permissionsAdmin = Permission::Where('name', 'like', 'employee%')
            ->orWhere('name', 'like', 'company%')
            ->pluck('id', 'id');
        $roleAdmin->syncPermissions($permissionsAdmin);



        $roleCompany = Role::create(['name' => 'company']);
        $permissionsCompany = Permission::Where('name', 'like', 'employee%')
            ->orWhere('name',  'company-list')
            ->orWhere('name',  'company-edit')
            ->pluck('id', 'id');
        $roleCompany->syncPermissions($permissionsCompany);


        $roleEmployee = Role::create(['name' => 'employee']);
        $permissionsEmployee = Permission::where('name', 'like', 'employee-list')
            ->orWhere('name', 'like', 'employee-edit')
            ->orWhere('name', 'like', 'company-list')
            ->pluck('id', 'id');
        $roleEmployee->syncPermissions($permissionsEmployee);



        //Asingining All roles to user
        $roles = Role::pluck('id', 'id')->all();

        $user->assignRole($role);
    }
}